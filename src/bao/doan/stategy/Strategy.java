package bao.doan.stategy;

public interface Strategy {
    public int doOperation(int num1, int num2);
}
